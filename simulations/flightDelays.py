from random import gauss
import simpy

# minutes per hour * hours per day * days per year
ENV_RUN_TIME = 60 * 24 * 365
DELAY_MINUTES_AVG = 10
DELAY_MINUTES_STD = 20
AT_AIRPORT_MINUTES = 60
CONNECTING_FLIGHT_WAIT_MINUTES = 120

FLIGHT_STATUS_FLYING = 'Flying'
FLIGHT_STATUS_AT_AIRPORT = 'At airport'


flightPath1 = [
    {'origin': 'DC', 'destination': 'Charlotte', 'avgFlightMinutes': 97},
    {'origin': 'Charlotte', 'destination': 'DC', 'avgFlightMinutes': 103},
]

flightPath2 = [
    {'origin': 'Charlotte', 'destination': 'Chicago', 'avgFlightMinutes': 128},
    {'origin': 'Chicago', 'destination': 'LA', 'avgFlightMinutes': 266},
    {'origin': 'LA', 'destination': 'Charlotte', 'avgFlightMinutes': 289},
]

def getFlightMinutes(avgFlightMinutes):
    delayMinutes = gauss(DELAY_MINUTES_AVG, DELAY_MINUTES_STD)
    return int(avgFlightMinutes + delayMinutes)


class Plane:
    def __init__(self, env, flightPath, connectingPlane=None):
        self.env = env
        self.potentialConnections = 0
        self.missedConnections = 0
        self.flightPath = flightPath
        self.connectingPlane = connectingPlane
        self.setFLightPath = self.setFLightPathGenerator()
        next(self.setFLightPath)
        self.status = FLIGHT_STATUS_AT_AIRPORT
        env.process(self.fly())

    def fly(self):
        while True:
            self.status = FLIGHT_STATUS_FLYING
            flightMinutes = getFlightMinutes(self.avgFlightMinutes)
            print(f'Flying from {self.origin} to {self.destination}. This will take {flightMinutes} minutes to complete.')
            yield self.env.timeout(flightMinutes)

            self.status = FLIGHT_STATUS_AT_AIRPORT
            print(f'Waiting at airport for {AT_AIRPORT_MINUTES} minutes')
            yield self.env.timeout(flightMinutes)

            if (
                self.connectingPlane
                and self.connectingPlane.status == FLIGHT_STATUS_FLYING
                and self.destination == 'Charlotte'
                and self.connectingPlane.destination == 'Charlotte'
            ):
                self.potentialConnections += 1
                yield self.env.timeout(CONNECTING_FLIGHT_WAIT_MINUTES)
                if self.connectingPlane.status != FLIGHT_STATUS_AT_AIRPORT:
                    self.missedConnections += 1

            next(self.setFLightPath)

    def setFLightPathGenerator(self):
        idx = 0
        while True:
            currentIdx = idx % len(self.flightPath)
            currentFlightPath = self.flightPath[currentIdx]
            self.origin = currentFlightPath['origin']
            self.destination = currentFlightPath['destination']
            self.avgFlightMinutes = currentFlightPath['avgFlightMinutes']
            yield
            idx += 1

env = simpy.Environment()
plane1 = Plane(env, flightPath1)
plane2 = Plane(env, flightPath2, plane1)
env.run(ENV_RUN_TIME)
print(f'Percent missed connections: {plane2.missedConnections / plane2.potentialConnections}')
print(f'Potential connections: {plane2.potentialConnections}')
print(f'Missed connections: {plane2.missedConnections}')
