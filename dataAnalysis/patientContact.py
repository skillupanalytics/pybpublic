from dataAnalysis import *
from importer import *
from importer.fileImport import FileImporter

defaultDataTypes = {
    'PatientID': parseIdToString,
    'OptedOut': parseBooleanString,
    'Call1Attempted': parseBooleanString,
    'Call1Answered': parseBooleanString,
    'Call1StartDt': safeDateTimeParse,
    'Call1EndDt': safeDateTimeParse,
    'Call2Attempted': parseBooleanString
}

# Combine multiple files
combinedRecords = []
for file in ('20210401', '20210402', '20210403'):
    fileImporter = FileImporter(f'{DATA_FILE_PATH}PatientContact_{file}.xlsx', defaultDataTypes=defaultDataTypes)
    combinedRecords += fileImporter.data

cleanRecords = []
patientIds = []
# Remove duplicates
# Filter for patients that didn't opt out
for patientRecord in combinedRecords:
    patientId = patientRecord['PatientID']
    if patientId in patientIds or patientRecord['OptedOut']:
        continue
    # Add call minutes
    patientRecord['Call1Minutes'] = getDateTimeDiff(patientRecord['Call1EndDt'], patientRecord['Call1StartDt'])
    cleanRecords.append(patientRecord)
    patientIds.append(patientId)

# Print to Excel file
sheetsConfig = [
    {'title': 'Call2List', 'data': cleanRecords, 'headers': list(cleanRecords[0].keys())},
    {'title': 'RawData', 'data': combinedRecords, 'headers': list(combinedRecords[0].keys())}
]
fileImporter.writeExcelFile('call2ContactList.xlsx', sheetsConfig=sheetsConfig)
print('Finished')