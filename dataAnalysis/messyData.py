from importer import *
from importer.fileImport import FileImporter

defaultDataTypes = {
    'startDate': safeDateTimeParse,
    'currentBalance': parseMoneyString
}

fileImporter = FileImporter(f'{DATA_FILE_PATH}MessyData.xlsx', defaultDataTypes=defaultDataTypes, noneStrings=('null', '-', 'empty'))
for record in fileImporter.data:
    lastName, firstName = splitName(record['fullName'])
    record['lastName'] = lastName
    record['firstName'] = firstName

fileImporter.printRows(5)

