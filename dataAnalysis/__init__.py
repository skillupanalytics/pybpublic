from datetime import datetime, date


TIME_AGG_SECONDS = 'seconds'
TIME_AGG_MINUTES = 'minutes'
TIME_AGG_HOURS = 'hours'
TIME_AGG_DAYS = 'days'

DATE_AGG_DAYS = 'days'
DATE_AGG_MONTHS = 'months'
DATE_AGG_QUARTERS = 'quarters'

def getDateAgg(dtValue, dateAgg=DATE_AGG_DAYS):
    if not isinstance(dtValue, (datetime, date)):
        return None

    if dateAgg == DATE_AGG_DAYS:
        if isinstance(dtValue, datetime):
            return dtValue.date()
        return dtValue
    if dateAgg == DATE_AGG_MONTHS:
        return dtValue.month
    if dateAgg == DATE_AGG_QUARTERS:
        month = dtValue.month
        if month < 4:
            return 'Q1'
        if month < 7:
            return 'Q2'
        if month < 10:
            return 'Q3'
        return 'Q4'
    raise ValueError(f'Unsupported dateAgg: {dateAgg}')

def getDateTimeDiff(endDateTime, startDateTime, timeAgg=TIME_AGG_MINUTES):
    if not endDateTime or not startDateTime:
        return None

    dateTimeDiff = endDateTime - startDateTime
    diffSecond = round(dateTimeDiff.total_seconds())
    if timeAgg == TIME_AGG_SECONDS:
        return diffSecond
    if timeAgg == TIME_AGG_MINUTES:
        return round(diffSecond / 60)
    if timeAgg == TIME_AGG_HOURS:
        return round(diffSecond / 60 / 60)
    if timeAgg == TIME_AGG_DAYS:
        return round(diffSecond / 60 / 60 / 24)

def getAgeGroup(age, ageGroupIncrement=10, ageLimit=80):
    startingAge = 0
    if age is None:
        return None
    while startingAge < ageLimit:
        endingAge = startingAge + ageGroupIncrement
        if age >= startingAge and age < endingAge:
            return f'{startingAge}-{endingAge}'
        startingAge = endingAge

    return f'{ageLimit}+'

def isChronological(ascendingDateTimeValues):
    for idx, dateTimeVal in enumerate(ascendingDateTimeValues[:-1]):
        nextDateTimeVal = ascendingDateTimeValues[idx + 1]
        if nextDateTimeVal < dateTimeVal:
            return False

    return True

