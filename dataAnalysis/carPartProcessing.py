from dataAnalysis import *
from importer import *
from importer.fileImport import FileImporter
from importer.groupStatistics import GroupStatistics

defaultDataTypes = {
    'processingStartTime': safeDateTimeParse,
    'processingEndTime': safeDateTimeParse,
    'subPartReadyTime': safeDateTimeParse,
}

fileImporter = FileImporter(f'{DATA_FILE_PATH}CarProcessingData_20201216_20201229.csv', defaultDataTypes=defaultDataTypes)
for record in fileImporter.data:
    record['processingMinutes'] = getDateTimeDiff(record['processingEndTime'], record['processingStartTime'])

groupedData = fileImporter.getGroupData([('machineType',)], isFlat=True)

statsByMachine = {key: GroupStatistics(machineRecords) for key, machineRecords in groupedData[0].items()}

print('Something')


