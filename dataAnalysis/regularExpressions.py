import re

from importer import *
from importer.fileImport import FileImporter

# https://docs.python.org/3/library/re.html

"""
^ = new line
\D = any non numeric character
*? = match the preceding value 0 or more times, but stop once the next part of the match is found
() = capture group
\d = any numeric character
{n} = match the preceding character n times
$ = end of the line

All together - Match a string with:
    - 0 or more non-number characters followed by...
    - exactly 3 numbers (capture these numbers in a group), followed by...
    - 0 or more non-number characters followed by...
    - exactly 3 numbers (capture these numbers in a group), followed by...
    - 0 or more non-number characters followed by...
    - exactly 4 numbers (capture these numbers in a group), followed by...
    - end of the string
"""

PHONE_NUMBER_PATTERN = '^\D*?(\d{3})\D*?(\d{3})\D*?(\d{4})$'


def phoneNumberParse(val):
    if phoneNumberMatch := re.match(PHONE_NUMBER_PATTERN, val):
        return '-'.join(phoneNumberMatch.groups())

    return None


"""
+? = match the preceding value 1 or more times, but stop once the next part of the match is found

All together - Match a string with:
    - "First name: " followed by...
    - one or more non-number characters followed by...
    - ", Last name: " followed by...
    - one or more non-number characters followed by...
    - end of the string
"""
NAME_PATTERN = '^First name: (\D+?), Last name: (\D+?)$'

"""
(?P<name>) = named capture group with the value assigned to whatever name is in <>
[0-9] = any digit from 0 to 9
{n, n} = lowest number of matches to highest number of matches
\W = any character that is not a number or letter
| = or

All together - Match a string with:
    - a number from 0 to 9 one or two times, assign the matched value to the month property, followed by...
    - any character that is not a number or letter one time, followed by
    - a number from 0 to 3 one time followed by a number from 0 to 9 one time, assign the matched value to the day property, followed by...
    - any character that is not a number or letter one time, followed by
    - a number from 0 to 9 two times or a number from 0 to 9 four times, assign the matched value to the year property, followed by...
    - end of the string
"""
BIRTHDAY_PATTERN = '^(?P<month>[0-9]{1,2})\W(?P<day>[0-3][0-9])\W(?P<year>[0-9]{2}|[0-9]{4})$'

"""
(?:) = non-capturing group
\s = white space character
? = match the preceding value 0 or 1 times
. = any character

All together - Match a string with:
    1) a white space 0 or 1 times followed by...
    2) any character 0 or more times, followed by...
    3) a comma
    4) the preceding pattern (steps 1-3) should repeat 2 to 3 times, but should not be captured in a group, followed by...
    5) a white space 0 or 1 times followed by...
    6) a non-number character 2 times, capture this in a group, followed by...
    7) any character 0 or more times, followed by...
    8) a number from 0 to 9 five times, capture this in a property called zipCode, followed by
    9) end of the string
"""

ADDRESS_PATTERN = '^(?:\s?.*?,){2,3}\s?(\D{2}).*?(?P<zipCode>[0-9]{5})$'

defaultDataTypes = {
    'phoneNumber': phoneNumberParse
}

fileImporter = FileImporter(f'{DATA_FILE_PATH}regexData.csv', defaultDataTypes=defaultDataTypes)
print('BEFORE----------------------------')
fileImporter.printRows(5)
for record in fileImporter.data:
    if nameMatch := re.match(NAME_PATTERN, record['name']):
        record['firstName'] = nameMatch.group(1)
        record['lastName'] = nameMatch.group(2)
    if birthdayMatch := re.match(BIRTHDAY_PATTERN, record['birthday']):
        record['birthYear'] = birthdayMatch.group('year')
    if addressMatch := re.match(ADDRESS_PATTERN, record['address']):
        record['zipCode'] = addressMatch.group('zipCode')
print('')
print('AFTER-----------------------------')
fileImporter.printRows(5)
