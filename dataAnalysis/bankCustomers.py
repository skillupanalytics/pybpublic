from dataAnalysis import *
from importer import *
from importer.fileImport import FileImporter

MAX_DEPOSIT_AMOUNT = 100000
LOYALTY_PROGRAMS = ['basic', 'silver', 'platinum']

defaultDataTypes = {
    'arrivalDt': safeDateTimeParse,
    'transactionDt': safeDateTimeParse,
    'departureDt': safeDateTimeParse,
    'depositAmount': float,
}


def isGoodRecordFn(record, idx):
    if not record['customerId']:
        print(f'On row {idx}: Bad customer id')
        return False
    if record['depositAmount'] > MAX_DEPOSIT_AMOUNT:
        print(f'On row {idx}: Exceeded max deposit amount of {MAX_DEPOSIT_AMOUNT}')
        return False
    if record['loyaltyProgram'] not in LOYALTY_PROGRAMS:
        print(f'On row {idx}: Not a valid loyalty program')
        return False
    if not isChronological([
        record['arrivalDt'],
        record['transactionDt'],
        record['departureDt']
    ]):
        print(f'On row {idx}: Timestamps are not in chronological order')
        return False

    return True


fileImporter = FileImporter(f'{DATA_FILE_PATH}bankCustomers_20210202.csv', defaultDataTypes=defaultDataTypes, isGoodRecordFn=isGoodRecordFn)
for badRecord in fileImporter.badRecords:
    print(badRecord)