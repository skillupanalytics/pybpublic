from datetime import datetime
from dateutil.parser import parse

DATA_FILE_PATH = r'/Users/pfb/PycharmProjects/pybpublic/dataFiles/'
DATA_FILE_OUTPUT_PATH = r'/Users/pfb/PycharmProjects/pybpublic/outputFiles/'

ROW_TYPE_DICT = 'dict'
ROW_TYPE_LIST = 'list'
ROW_TYPE_TUPLE = 'tuple'

FILE_TYPE_CSV = 'csv'
FILE_TYPE_XLS = 'xls'

DEFAULT_NONE_STRINGS = ('null', 'na', 'n/a')


def safeDateTimeParse(val):
    if not val:
        return None
    if isinstance(val, datetime):
        return val
    return parse(val)

def parsePercentageString(val):
    return float(val.replace('%', ''))

def parseIntegerString(val):
    return int(val.replace(',', ''))

def parseBooleanString(val):
    try:
        val = val.lower()
    except SyntaxError:
        pass

    if val in (0, 'no', 'false', '0'):
        return False
    return True

def parseIdToString(val):
    if val is None:
        return val
    return str(int(val))

def parseMoneyString(val):
    try:
        return float(val.replace('$', '').replace(',', ''))
    except AttributeError:
        return None

def splitName(fullName):
    try:
        return [name.strip() for name in fullName.split(',')]
    except AttributeError:
        return None